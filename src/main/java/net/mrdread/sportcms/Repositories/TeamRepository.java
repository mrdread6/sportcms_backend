package net.mrdread.sportcms.Repositories;

import net.mrdread.sportcms.Entities.League.TeamEntity;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeamRepository extends JpaRepository<TeamEntity, Long> {
    @Modifying
    @Query("DELETE TeamEntity t WHERE t.id = :id")
    int customDeleteById(@Param("id") Long id);
    List<TeamEntity> findAllByOrderById(PageRequest page);
}
