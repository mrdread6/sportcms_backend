package net.mrdread.sportcms.Repositories;

import net.mrdread.sportcms.Entities.ArticleEntity;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ArticleRepository extends JpaRepository<ArticleEntity, Long> {
    List<ArticleEntity> findAllByOrderByPublishDateDesc(PageRequest of);
}
