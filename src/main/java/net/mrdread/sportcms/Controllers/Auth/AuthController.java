package net.mrdread.sportcms.Controllers.Auth;

import lombok.RequiredArgsConstructor;
import net.mrdread.sportcms.Entities.Auth.AuthenticationRequest;
import net.mrdread.sportcms.Entities.Auth.AuthenticationResponse;
import net.mrdread.sportcms.Entities.Auth.RegisterRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
public class AuthController {
    private final AuthenticationService service;


    @PostMapping("/register")
    public ResponseEntity<AuthenticationResponse> register(
            @RequestBody RegisterRequest request
            ){
        return ResponseEntity.ok(service.register(request));
    }
    @PostMapping("/authenticate")
    public ResponseEntity<AuthenticationResponse> SignIn(
            @RequestBody AuthenticationRequest request
    ){
        try {
            return ResponseEntity.ok(service.authenticate(request));
        } catch ( Exception e ) {
            throw new RuntimeException(e);
        }
    }

}
