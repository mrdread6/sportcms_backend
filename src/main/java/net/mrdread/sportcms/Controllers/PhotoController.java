package net.mrdread.sportcms.Controllers;

import net.mrdread.sportcms.Entities.PhotoEntity;
import net.mrdread.sportcms.Repositories.PhotoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;

@RestController()
@RequestMapping("/api/v1")
public class PhotoController {

    @Autowired
    PhotoRepository photoRepository;
    @RequestMapping("/public/photo/show/{photoId}")
    public ResponseEntity<?> GetPhoto(@PathVariable Long photoId){
        Optional<PhotoEntity> photoInfo = photoRepository.findById(photoId);
        if( photoInfo.isEmpty() )
            return  ResponseEntity.badRequest().build();

        Optional<String> filePath = photoInfo.map(PhotoEntity::getPath);

        if( filePath.isEmpty() )
            return ResponseEntity.badRequest().build();

        ResponseEntity<Resource> file = StorageController.GetFile(filePath.get());
        if(file == null)
            return ResponseEntity.badRequest().build();
        return file;
    }
    @PostMapping("/photo/upload")
    public ResponseEntity<?> UploadPhoto(@RequestParam("file") MultipartFile file){
        String newFileName = null;
        try {
            newFileName = StorageController.SaveFile(file);
        } catch ( IOException e ) {
            System.out.println(e.getMessage());
            return  ResponseEntity.badRequest().build();
        }
        if ( newFileName == null)
            return ResponseEntity.badRequest().build();

        PhotoEntity newPhoto = new PhotoEntity();
        newPhoto.setPath(newFileName);
        photoRepository.save(newPhoto);

        return ResponseEntity.ok(newPhoto);
    }

    @PostMapping("/photo/delete/{photoId}")
    public ResponseEntity<?> RemovePhoto(@PathVariable Long photoId){
        try {
            Optional<PhotoEntity> photoInfo = photoRepository.findById(photoId);
            if(photoInfo.isEmpty())
                return ResponseEntity.badRequest().build();

            Optional<String> filePath =  photoInfo.map(PhotoEntity::getPath);

            if(filePath.isEmpty())
                return ResponseEntity.internalServerError().build();

            photoRepository.deleteById(photoId);
            StorageController.RemoveFile(filePath.get());
        } catch ( Exception e ){
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok().build();
    }
}
