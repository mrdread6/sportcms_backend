package net.mrdread.sportcms.Controllers;

import net.mrdread.sportcms.Entities.ArticleEntity;
import net.mrdread.sportcms.Repositories.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1")
public class ArticleController {
    @Autowired
    private ArticleRepository articleRepository;

    private PhotoController photoController;

    @GetMapping("/public/articles")
    public List<ArticleEntity> GetArticlesFromTo(@RequestParam("page") int page) {
        return articleRepository.findAllByOrderByPublishDateDesc(PageRequest.of(page, 10));
    }

    @GetMapping("/public/article")
    public Optional<ArticleEntity> GetArticleById(@RequestParam("articleID") Long id) {
        return articleRepository.findById(id);
    }

    @PostMapping("/article/create")
    public ResponseEntity<?> CreateArticle(@RequestParam("article") String articleContent,
                                           @RequestParam("articleCreator") String articleCreator,
                                           @RequestParam("photos") MultipartFile[] files) {
        try {
            ArticleEntity newArticle = new ArticleEntity();
            newArticle.setCreatorID(Long.valueOf(articleCreator));
            newArticle.setContent(articleContent);
            newArticle.setPublishDate(ZonedDateTime.now());

            Path uploadLocation = Paths.get("./uploads").toAbsolutePath().normalize();

            if (!uploadLocation.toFile().exists() || !uploadLocation.toFile().isDirectory())
                uploadLocation.toFile().mkdirs();

            for ( MultipartFile file : files ) {
                ResponseEntity<?> responseEntity = photoController.UploadPhoto(file);
                if(!responseEntity.equals(ResponseEntity.ok().build()))
                    return responseEntity;
            }

        } catch ( Exception e ) {
            return ((ResponseEntity<?>) ResponseEntity.badRequest());
        }

        return ResponseEntity.ok("Article Created");
    }
}
