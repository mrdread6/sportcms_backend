package net.mrdread.sportcms.Controllers;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.UUID;

public final class StorageController {
    private static final Path uploadsDirectory = Paths.get("./uploads").toAbsolutePath().normalize();
    public static ResponseEntity<Resource> GetFile(String fileName){
        try{
            if( !DirExists() ) {
                return null;
            }

            Path filePath = uploadsDirectory.resolve(fileName).normalize();

            if(!filePath.toFile().exists())
                return ResponseEntity.notFound().build();

            Resource image = new UrlResource(filePath.toUri());
            return ResponseEntity.ok()
                    .contentType(MediaType.IMAGE_JPEG)
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + image.getFilename() + "\"")
                    .body(image);

        } catch (Exception e){
            return ResponseEntity.internalServerError().build();
        }
    }

    public static String SaveFile(MultipartFile file) throws IOException {
        StringBuilder fileName = new StringBuilder(UUID.randomUUID().toString());
        File newFile;

        if(!DirExists())
            uploadsDirectory.toFile().mkdirs();

        String fileType = file.getContentType();

        switch (fileType) {
            case "image/png" -> fileName.append(".png");
            case "image/jpeg" -> fileName.append(".jpg");
            case "image/svg+xml" -> fileName.append(".svg");
            case null, default -> {
                return null;
            }
        }
        newFile = new File(uploadsDirectory.toString(), fileName.toString());
        file.transferTo(newFile.toPath());
        return fileName.toString();
    }

    public static ResponseEntity<?> RemoveFile(String fileName) throws Exception{
            if(!DirExists())
                return ResponseEntity.internalServerError().build();

            Path filePath = uploadsDirectory.resolve(fileName).normalize();

            if(!filePath.toFile().exists())
                return ResponseEntity.notFound().build();

            Files.delete(filePath);

            return ResponseEntity.ok().build();
    }
    private static boolean DirExists(){
        if(!uploadsDirectory.toFile().exists())
            return false;
        return uploadsDirectory.toFile().isDirectory();
    }
}
