package net.mrdread.sportcms.Controllers;

import jakarta.transaction.Transaction;
import jakarta.transaction.Transactional;
import net.mrdread.sportcms.Entities.League.TeamEntity;
import net.mrdread.sportcms.Repositories.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1")
@Transactional
public class TeamsController {
    @Autowired
    TeamRepository teamsRepository;
    @GetMapping("/public/team/{teamId}")
    public Optional<TeamEntity> GetTeam(@PathVariable Long teamId){
        return teamsRepository.findById(teamId);
    }
    @PostMapping("/team/create")
    public ResponseEntity<?> CreateTeam(@RequestParam String shortName,
                                        @RequestParam String name,
                                        @RequestParam MultipartFile logo){
        TeamEntity tmp = new TeamEntity();
        String newFileName;
        tmp.setName(name);
        tmp.setShortName(shortName);
        try {
           newFileName = StorageController.SaveFile(logo);
        } catch ( IOException e ) {
            return ResponseEntity.badRequest().build();
        }
        System.out.println(newFileName);
        tmp.setLogoPath(newFileName);
        teamsRepository.save(tmp);
        return ResponseEntity.ok().build();
    }
    @GetMapping("/public/teams")
    public List<TeamEntity> GetTeamsByPage(@RequestParam("page") int page, @RequestParam("size") int size){
        return teamsRepository.findAllByOrderById(PageRequest.of(page,size));
    }

    @PostMapping("/team/delete")
    public ResponseEntity<?> DeleteEntity(@RequestParam("id") Long id){
        int count = teamsRepository.customDeleteById(id);
        if(count == 1){
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.badRequest().build();
    }
}
