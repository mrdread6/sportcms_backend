package net.mrdread.sportcms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SportcmsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SportcmsApplication.class, args);
	}

}
