package net.mrdread.sportcms.Entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.ZonedDateTime;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "articles", schema = "public")
public class ArticleEntity implements Comparable<ArticleEntity> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "content")
    private String content;

    @Column(name = "creatorID")
    private Long creatorID;

    @Column(name = "publishDate")
    private ZonedDateTime publishDate;

    @OneToMany
    private List<PhotoEntity> photos;
    @Override
    public int compareTo(ArticleEntity arg0) {
        return this.publishDate.compareTo(arg0.publishDate);
    }
}
