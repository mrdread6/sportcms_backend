package net.mrdread.sportcms.Entities.Auth;

import lombok.*;
import net.mrdread.sportcms.Entities.Role;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegisterRequest {
    private String email;
    private String password;
    private String firstname;
    private String lastname;
    private Role role;
}
