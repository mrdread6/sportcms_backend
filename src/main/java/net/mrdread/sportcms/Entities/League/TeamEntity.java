package net.mrdread.sportcms.Entities.League;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "teams", schema = "public")
public class TeamEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "short")
    private String shortName;
    @Column(name = "name")
    private String name;
    @Column(name = "logo")
    private String logoPath;
}
