package net.mrdread.sportcms.Entities.League;

import java.time.ZonedDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import net.mrdread.sportcms.Entities.League.TeamEntity;

@Entity
@Getter
@Setter
@AllArgsConstructor
@Table(name = "matches", schema = "public")
public class Match{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private TeamEntity teamA;

    @ManyToOne
    private TeamEntity teamB;

    @Column(name = "scoreA")
    private int scoreA;

    @Column(name = "scoreB")
    private int scoreB;

    @Column(name = "date")
    private ZonedDateTime zonedDateTime;
}
